 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file = "Header.jsp"%>
<title>Publication</title>
</head>
<body>

<table>
<tr>
<th>Id</th><th>Title</th><th>Publication Name</th><th>Publication Date</th><th>Authors</th><th>Cite Count</th>
</tr>
<tr>
				<td>${pi.id} </td>
                <td>${pi.title}</td>
                <td>${pi.publicationName}</td>
                <td>${pi.publicationDate}</td>
                <td>${pi.authors}</td>
                <td>${pi.citeCount}</td>
</tr>
</table>

</body>
</html>